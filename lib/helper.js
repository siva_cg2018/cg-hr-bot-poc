exports.greetPreefix = function(){
	d = new Date();
	h = d.getHours();
	if(parseInt(h)<12){
		return "Good Morning"
	}else if(parseInt(h) < 17){
		return "Good Afternoon"
	}else{
		return "Good Evening"
	}
};

exports.suggestionButtons = function(session, builder,text,buttons){
	button_schema = new Array();
	for(i=0; i<buttons.length;i++){
		b = buttons[i];
		b_text = b["label"];
		print_text = b["print"]
		button_schema.push(builder.CardAction.imBack(session,print_text,b_text));
	}
	return new builder.Message(session)
	.text(text)
	.suggestedActions(
		builder.SuggestedActions.create(session,button_schema)
	)
}