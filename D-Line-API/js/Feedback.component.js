System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var Feedback;
    return {
        setters:[],
        execute: function() {
            Feedback = (function () {
                function Feedback(power) {
                    this.power = power;
                }
                return Feedback;
            }());
            exports_1("Feedback", Feedback);
        }
    }
});

//# sourceMappingURL=Feedback.component.js.map
