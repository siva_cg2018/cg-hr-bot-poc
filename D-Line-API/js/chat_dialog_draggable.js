/*    ----------------------------------------Adding HTML------------------------------- */
var codeBlock = '<div class="chatbot-drag-container"></div>'+
'<div class="chatbot-button is-visible">'+
'<h2 class="chatbot-header-text">Need help?</h2></div>'+
'<div class="chatbot is-hidden transitions-active">'+
'<div class="chatbot-header">'+
'<h2 class="chatbot-header-text">Ask Frankie</h2>'+
'<div class="float-right"><i class="chatbot-minimize" style="margin-top: 6px;"></i><i class="chatbot-close"></i></div></div>'+
'<div class="chatbot-panel">'+
'<iframe class="chatbot-iframe" src="../dist/index.html"></iframe>'+
'</div></div>';
$(document).ready(function(){
  $(".wrapper").html(codeBlock);
});
/*   --------------------------------------------Ending HTML--------------------------------------------------  */