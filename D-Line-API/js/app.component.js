System.register(['angular2/core', './dialog.response', './dialog.service', 'angular2/http', './ce.docs', './payload', 'angular2/common', './Feedback.component'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, dialog_response_1, dialog_service_1, http_1, ce_docs_1, payload_1, common_1, Feedback_component_1;
    var AppComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (dialog_response_1_1) {
                dialog_response_1 = dialog_response_1_1;
            },
            function (dialog_service_1_1) {
                dialog_service_1 = dialog_service_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (ce_docs_1_1) {
                ce_docs_1 = ce_docs_1_1;
            },
            function (payload_1_1) {
                payload_1 = payload_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (Feedback_component_1_1) {
                Feedback_component_1 = Feedback_component_1_1;
            }],
        execute: function() {
            /*
             * Main entry point to the application. This component is responsible for the entire page layout.
             */
            AppComponent = (function () {
                function AppComponent(_dialogService, http) {
                    var _this = this;
                    this._dialogService = _dialogService;
                    this.http = http;
                    // Store the response so we can display the JSON for end user to see
                    // We will also need to use the response's context for subsequent calls
                    this.response = null;
                    this.buttons_text_splitted_array = [];
                    this.timer = null;
                    this.setupTimer = null;
                    this.setupTimer1 = null;
                    this.question = null;
                    this.segments = []; // Array of requests and responses
                    this.workspace_id = null;
                    this.power = ['5-Excellent', '4-Great', '3-Good', '2-Fair', '1-Poor'];
                    this.myCar = new Feedback_component_1.Feedback(this.power[0]);
                    this.submitted = false;
                    /*--------------------------------------------- Reload Iframe ---------------------------------------*/
                    this.reload = function (_dialogService) {
                        _this.checkSetup(_dialogService);
                    };
                    /*-------------------------------------------- Print Content of Iframe-------------------------------*/
                    this.print = function () {
                        window.print();
                    };
                    this.getLang();
                    this.model = {
                        rating: '0 star'
                    };
                }
                /* private checkFocus() {
                let landscape = jQuery(window.parent.document.getElementById('media')).css('margin-left');
                if (/iPad/.test(navigator.userAgent) && landscape === '450px') {
                let v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
                let ver = [parseInt(v[1], 10)];
                if (ver[0] >= 10) {
                jQuery(window.parent.document.getElementsByClassName('container_id')).removeAttr('style');
                jQuery(window.parent.document.getElementsByClassName('container_id')).css('width', '280pt');
                jQuery(window.parent.document.getElementsByClassName('container_id')).css('padding', '0px');
                jQuery(window.parent.document.getElementById('wrapper_ipad')).css('position', 'absolute');
                jQuery(window.parent.document.getElementById('wrapper_ipad')).css('bottom', '10px');
                jQuery(window.parent.document.getElementById('chatbotId')).removeAttr('style');
                jQuery(window.parent.document.getElementById('chatbotId')).css('position', 'relative');
                jQuery(window.parent.document.getElementById('chatbotId')).css('bottom', '-167px');
                jQuery(window.parent.document.getElementById('chatbotId')).css('width', '280pt');
                jQuery(window.parent.document.getElementById('chatbotId')).css('padding', '0px');
                jQuery(window.parent.document.getElementById('chatbotId')).css('margin-bottom', '-20px');
                }
                }
                }
                private lost_checkFocus() {
                let landscape = jQuery(window.parent.document.getElementById('media')).css('margin-left');
                if (/iPad/.test(navigator.userAgent) && landscape === '450px') {
                let v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
                let ver = [parseInt(v[1], 10)];
                if (ver[0] >= 10) {
                jQuery(window.parent.document.getElementsByClassName('container_id')).removeAttr('style');
                jQuery(window.parent.document.getElementsByClassName('container_id')).css('width', '280pt');
                jQuery(window.parent.document.getElementsByClassName('container_id')).css('position', 'fixed');
                jQuery(window.parent.document.getElementsByClassName('container_id')).css('padding', '0px');
                jQuery(window.parent.document.getElementById('wrapper_ipad')).removeAttr('style');
                jQuery(window.parent.document.getElementById('chatbotId')).removeAttr('style');
                jQuery(window.parent.document.getElementById('chatbotId')).css('position', 'fixed');
                jQuery(window.parent.document.getElementById('chatbotId')).css('bottom', '-20px');
                jQuery(window.parent.document.getElementById('chatbotId')).css('width', '280pt');
                jQuery(window.parent.document.getElementById('chatbotId')).css('padding', '0px');
                }
                }
                } */
                /*-------------------------------------------------------- Submit Feedback Value --------------------------------*/
                AppComponent.prototype.onSubmit = function (data) {
                    this.submitted = true;
                    var chatColumn = document.querySelector('#scrollingChat');
                    chatColumn.classList.add('loading');
                    this.model = JSON.stringify(this.model);
                    var context = null;
                    if (this.response != null) {
                        context = this.response.context;
                        // we are going to delete the context variable 'callRetrieveAndRank' before
                        // sending back to the Conversation service
                        if (context && context.callRetrieveAndRank) {
                            delete context.callRetrieveAndRank;
                        }
                    }
                    var inputString = this.model;
                    /* --------------------- Split Rating string --------------------------  */
                    var inputString_split = inputString.split('"');
                    var input = { 'text': inputString_split[1] + '-' + inputString_split[3] };
                    /*  --------------------- Split Rating string --------------------------  */
                    var payloaddata = { input: input, context: context };
                    this.callConversationService(chatColumn, payloaddata);
                    /* Hides modal after submit */
                    jQuery('#myModal').modal('hide');
                    jQuery('#li_tag').attr('disabled', 'true');
                    /* Hide Modal only one after submit
                    jQuery('.modal').removeClass('in');
                    jQuery('.modal').attr('aria-hidden', 'true');
                    jQuery('.modal').css('display', 'none');
                    jQuery('.modal-backdrop').remove();
                    jQuery('body').removeClass('modal-open');
                    return false; */
                };
                /*------------------------------------------ Sendmail Content of Iframe-------------------------------*/
                AppComponent.prototype.sendMail = function () {
                    var iframe = '';
                    for (var i = 0; i < this.segments.length; i++) {
                        var splitted = this.segments[i].getTime().split(',');
                        if (this.segments[i].isUser()) {
                            iframe = iframe + '%5B' + splitted[1] + '%20at%20' + splitted[0] + '%20%5D' + '%20%20' + 'You%20:%20%20'
                                + this.segments[i].getText() + '%0D%0A';
                        }
                        else {
                            iframe = iframe + '%5B' + splitted[1] + '%20at%20' + splitted[0] + '%20%5D' + '%20%20' + 'Bot%20:%20%20'
                                + this.segments[i].getText() + '%0D%0A';
                        }
                    }
                    var link = 'mailto:email@domain.com?subject=Chatbot Backup'
                        + '&body=Hi%20,%0D%0A%0D%0APlease%20find%20your%20chat%20transcript%20as%20below.%0D%0A%0D%0A' +
                        iframe + '%0D%0A%0D%0AThanks%20%26%20Regards%2C%0D%0A%0D%0AAGH%20Team.';
                    window.top.location.href = link;
                };
                /*
                 * This method is responsible for detecting user locale and getting locale specific content to be displayed by making a
                 * GET request to the respective file.
                 */
                AppComponent.prototype.getLang = function () {
                    var _this = this;
                    var browserLang = window.navigator.language || window.navigator.userLanguage;
                    var complLang = browserLang.split('-');
                    var time;
                    var id_counter;
                    var buttons_required;
                    var buttons_text_splitted_array;
                    var lang = complLang[0];
                    var lang_url = 'locale/' + lang + '.json';
                    this.http.get(lang_url).map(function (res) { return res.json(); }).subscribe(function (data) {
                        _this.langData = data;
                    }, function (error) {
                        var lang_url = 'locale/en.json';
                        _this.http.get(lang_url).map(function (res) { return res.json(); }).subscribe(function (data) {
                            _this.langData = data;
                            _this.segments.push(new dialog_response_1.DialogResponse(_this.langData.Description, false, null, null, time, buttons_required, buttons_text_splitted_array, id_counter));
                        }, function (error) { return alert(JSON.stringify(error)); });
                    });
                };
                AppComponent.prototype.ngAfterViewInit = function (_dialogService) {
                    this.checkSetup(_dialogService);
                    // suraj added to hide the right column all together!
                    // let rightColumn = <HTMLElement>document.querySelector ('.right');
                    // this.resizePayloadColumn (rightColumn);
                };
                /*
                 * This method is responsible for detecting if the set-up processs involving creation of various Watson services
                 * and configuring them is complete. The status is checked every one minute till its complete.
                 * A loading screen is displayed to show set-up progress accordingly.
                 */
                AppComponent.prototype.checkSetup = function (_dialogService) {
                    var _this = this;
                    this._dialogService.setup().subscribe(function (data) {
                        _this.workspace_id = data.WORKSPACE_ID;
                        var setup_state = data.setup_state;
                        var setup_status_msg = data.setup_status_message;
                        var setup_phase = data.setup_phase;
                        var setup_message = data.setup_message;
                        var setup_step = data.setup_step;
                        var setup = document.querySelector('.setup');
                        var setup_status = document.querySelector('.setup-msg');
                        var chat_app = document.querySelector('chat-app');
                        var setupLoader = document.querySelector('.setup-loader');
                        var setupPhase = document.querySelector('.setup-phase');
                        var setupPhaseMsg = document.querySelector('.setup-phase-msg');
                        var errorPhase = document.querySelector('.error-phase');
                        var errorPhaseMsg = document.querySelector('.error-phase-msg');
                        var circles = document.querySelector('.circles');
                        var gerror = document.querySelector('.gerror');
                        var werror = document.querySelector('.werror');
                        var activeCircle = document.querySelector('.active-circle');
                        var nactiveCircle = document.querySelector('.non-active-circle');
                        setup_status.innerHTML = setup_status_msg;
                        if (setup_state === 'not_ready') {
                            document.body.style.backgroundColor = 'darkgray';
                            chat_app.style.opacity = '0.25';
                            setup.style.display = 'block';
                            setupPhase.innerHTML = setup_phase;
                            setupPhaseMsg.innerHTML = setup_message;
                            if (setup_step === '0') {
                                errorPhase.innerHTML = setup_phase;
                                errorPhaseMsg.innerHTML = setup_message;
                                setupLoader.style.display = 'none';
                                setupPhase.style.display = 'none';
                                setupPhaseMsg.style.display = 'none';
                                circles.style.display = 'none';
                                if (setup_phase !== 'Error') {
                                    werror.style.display = 'block';
                                }
                                else {
                                    gerror.style.display = 'block';
                                }
                                errorPhase.style.display = 'block';
                                errorPhaseMsg.style.display = 'block';
                            }
                            else {
                                setupLoader.style.display = 'block';
                                setupPhase.style.display = 'block';
                                setupPhaseMsg.style.display = 'block';
                                circles.style.display = 'block';
                                gerror.style.display = 'none';
                                werror.style.display = 'none';
                                errorPhase.style.display = 'none';
                                errorPhaseMsg.style.display = 'none';
                            }
                            if (setup_step === '2') {
                                activeCircle.classList.remove('active-circle');
                                activeCircle.classList.add('non-active-circle');
                                nactiveCircle.classList.remove('non-active-circle');
                                nactiveCircle.classList.add('active-circle');
                            }
                            _this.setupTimer = setTimeout(function () {
                                _this.checkSetup(_dialogService);
                            }, 60000);
                        }
                        else {
                            var payload = { 'input': { 'text': '' } };
                            var chatColumn = document.querySelector('#scrollingChat');
                            _this.callConversationService(chatColumn, payload);
                            // Replaced bg color with image
                            // document.body.style.backgroundColor = 'white';
                            chat_app.style.opacity = '1';
                            setup.style.display = 'none';
                            if (_this.setupTimer) {
                                clearTimeout(_this.setupTimer);
                            }
                        }
                    }, function (error) { return alert(JSON.stringify(error)); });
                };
                AppComponent.prototype.onResize = function (event) {
                    // let rightColumn = <HTMLElement>document.querySelector ('.right');
                    // this.resizePayloadColumn (rightColumn);
                };
                /*
                 * This method is responsible for toggling Expand/Collapse section of CE content.
                 */
                AppComponent.prototype.CeToggle = function (event) {
                    var targetElement;
                    if (event.srcElement) {
                        targetElement = event.srcElement;
                    }
                    else {
                        targetElement = event.target;
                    }
                    if (targetElement.className === 'sign') {
                        targetElement = targetElement.parentElement;
                    }
                    if (targetElement.innerText.indexOf('Collapse') !== -1) {
                        targetElement.innerHTML = this.langData.EResults + '<span class=sign>+</span>';
                        targetElement.style.border = '';
                        targetElement.title = this.langData.Expand;
                    }
                    else {
                        targetElement.innerHTML = this.langData.CResults + '<span class=sign>-</span>';
                        targetElement.style.border = 'none';
                        targetElement.title = this.langData.Collapse;
                    }
                    var expcoll = targetElement.nextElementSibling;
                    if (expcoll && (expcoll.style.display === 'block' || expcoll.style.display === '')) {
                        expcoll.style.display = 'none';
                    }
                    else {
                        expcoll.style.display = 'block';
                    }
                };
                /*
                 * This method is responsible for triggering a request whenever a Enter key is pressed .  || event.which === 1
                 */
                AppComponent.prototype.keypressed = function (event) {
                    var element = document.querySelector('.draw');
                    var nw = element.offsetWidth + 7;
                    if (event && event.keyCode === 8) {
                        if (this.question === null || this.question === '') {
                            return false;
                        }
                        else {
                            nw = element.offsetWidth - 7;
                        }
                    }
                    if (nw > 360) {
                        nw = 360;
                    }
                    element.style.width = String(nw + 'px');
                    // On enter sendData/On button click
                    if (event && event.keyCode === 13 || event.which === 1) {
                        // To check input tag and dont submit if null
                        if (this.question === null || this.question === '') {
                            return false;
                        }
                        else {
                            this.sendData();
                            element.style.width = '0px';
                        }
                        event.preventDefault();
                    }
                };
                /*
                 * This method is responsible for changing the layout of payload section based on screen resolution.
                 */
                AppComponent.prototype.resizePayloadColumn = function (rightColumn) {
                    if (window.innerWidth < 730) {
                        rightColumn.classList.add('no-show');
                    }
                    else if (window.innerWidth < 830) {
                        rightColumn.classList.remove('no-show');
                        rightColumn.style.width = '340px';
                    }
                    else if (window.innerWidth < 860) {
                        rightColumn.classList.remove('no-show');
                        rightColumn.style.width = '445px';
                    }
                    else if (window.innerWidth < 951) {
                        rightColumn.classList.remove('no-show');
                        rightColumn.style.width = '395px';
                    }
                    else {
                        rightColumn.classList.remove('no-show');
                        rightColumn.style.width = '445px';
                    }
                };
                /*
                 * This method is responsible for toggling the payload section to full screen or fixed layout by
                 * clicking the Code Expand/Collapse icons at the top right .
                 */
                AppComponent.prototype.togglePanel = function (event) {
                    /* let payloadColumn = <HTMLElement>document.querySelector ('#payload-column');
                    let toggleButton = <HTMLElement>document.querySelector ('#view-change-button');
                    let rightColumn = <HTMLElement>document.querySelector ('.right');
                    let element;
                    if (event.srcElement) {
                      element = event.srcElement;
                    } else {
                      element = event.target;
                    }
                    if (toggleButton.classList.contains ('full')) {
                      toggleButton.classList.remove ('full');
                      payloadColumn.classList.remove ('full');
                      this.resizePayloadColumn (rightColumn);
                    } else {
                      rightColumn.classList.remove ('no-show');
                      rightColumn.style.width = '100%';
                      toggleButton.classList.add ('full');
                      payloadColumn.classList.add ('full');
                    }
                     */
                };
                /*
                 * This method is responsible for preparing the data to send and call the method for Conversation Service
                 */
                AppComponent.prototype.sendData = function () {
                    var chatColumn = document.querySelector('#scrollingChat');
                    chatColumn.classList.add('loading');
                    // Get timestamp for every response
                    var d = new Date();
                    var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July',
                        'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                    var id_counter = ('0' + d.getMinutes()).slice(-2) + ('0' + d.getSeconds()).slice(-2);
                    var today = ('0' + d.getHours()).slice(-2) + ':' + ('0' + d.getMinutes()).slice(-2) +
                        ',' + ' ' + monthNames[d.getMonth()] + ' ' + d.getDate();
                    var q = '';
                    var buttons_required;
                    var buttons_text_splitted_array;
                    if (this.question != null) {
                        q = this.question;
                    }
                    this.question = null;
                    var context = null;
                    if (this.response != null) {
                        context = this.response.context;
                        // we are going to delete the context variable 'callRetrieveAndRank' before
                        // sending back to the Conversation service
                        if (context && context.callRetrieveAndRank) {
                            delete context.callRetrieveAndRank;
                        }
                    }
                    var input = { 'text': q };
                    var payload = { input: input, context: context };
                    var time;
                    // Add the user utterance to the list of chat segments
                    this.segments.push(new dialog_response_1.DialogResponse(q, true, null, payload, today, buttons_required, buttons_text_splitted_array, id_counter));
                    this.timer = setTimeout(function () {
                        var msgDiv = document.getElementById('scrollingChat');
                        // Display Message before watson response
                        msgDiv.scrollTop = msgDiv.scrollHeight;
                        var messages = document.getElementById('scrollingChat').getElementsByClassName('clear');
                    }, 50);
                    // Call the method which calls the proxy for the message api
                    this.callConversationService(chatColumn, payload);
                };
                /*--------------------------------------Checks Value Before modal Open-------------------------------*/
                AppComponent.prototype.checkModal = function (responseText) {
                    if (responseText === true) {
                        this.submitted = false;
                        this.model = {
                            rating: '0 star'
                        };
                        jQuery('#myModal').modal('show');
                    }
                };
                /*-------------------------------------- Checks Button Value -------------------------------*/
                AppComponent.prototype.button_click = function (data, button_id) {
                    jQuery('#' + button_id).css({
                        'background': '#009522',
                        'color': 'white'
                    });
                    this.question = data;
                    this.keypressed(event);
                    jQuery('#textInput').removeAttr('disabled');
                    jQuery('#textInput').focus();
                    jQuery('#disable_button').attr('disabled', 'disabled');
                };
                /*
                * This method is responsible for making a request to Conversation service with the corresponding user query.
                */
                AppComponent.prototype.callConversationService = function (chatColumn, payload) {
                    var _this = this;
                    var responseText = '';
                    var ce = null;
                    var buttons_required;
                    var buttons_text_splitted_array = null;
                    var buttons_text_counter = null;
                    // Get timestamp for every response
                    var d = new Date();
                    var feedback_value = false;
                    var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July',
                        'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                    var id_counter = ('0' + d.getMinutes()).slice(-2) + ('0' + d.getSeconds()).slice(-2);
                    var today = ('0' + d.getHours()).slice(-2) + ':' + ('0' + d.getMinutes()).slice(-2) +
                        ',' + ' ' + monthNames[d.getMonth()] + ' ' + d.getDate();
                    // Before calling conversation, set the call_retrieve_and_rank flag to false
                    // conversation and not the app should control when retrieve and rank is called
                    if (payload.context) {
                        payload.context.call_retrieve_and_rank = false;
                    }
                    // ..........Start Reload Functionality..........
                    if (payload.input.text === '') {
                        this.segments.length = 0;
                        jQuery('.success').show();
                        setTimeout(function () {
                            jQuery('.success').hide();
                        }, 3000);
                    }
                    // ..........End Reload Functionality..........
                    // Send the user utterance to dialog, also send previous context
                    this._dialogService.message(this.workspace_id, payload).subscribe(function (data1) {
                        _this.response = data1;
                        if (data1) {
                            if (data1.error) {
                                responseText = data1.error;
                                data1 = _this.langData.NResponse;
                            }
                            else if (data1.output) {
                                if (data1.output.CEPayload && data1.output.CEPayload.length > 0) {
                                    ce = data1.output.CEPayload;
                                    responseText = _this.langData.Great;
                                }
                                else if (data1.output.text) {
                                    responseText = data1.output.text.length >= 1 && !data1.output.text[0] ? data1.output.text.join(' ').trim() : data1.output.text[0]; // tslint:disable-line max-line-length
                                }
                            }
                        }
                        buttons_required = _this.response.context.buttons_required;
                        var buttons_text = _this.response.context.buttonsText;
                        if (buttons_text != null) {
                            buttons_text_splitted_array = buttons_text.split(',');
                            _this.response.context.buttons_required = null;
                            _this.response.context.buttonsText = null;
                        }
                        for (var i = 0; i < buttons_required; i++) {
                            var autoButton = document.createElement('button');
                            jQuery('#' + id_counter).append(autoButton);
                            jQuery('#textInput').attr('disabled', 'disabled');
                        }
                        // Check feedback value true or false
                        feedback_value = _this.response.context.feedback;
                        var msgDiv = document.getElementById('scrollingChat');
                        var previousscrollHeight = msgDiv.scrollHeight;
                        _this.segments.push(new dialog_response_1.DialogResponse(responseText, false, ce, data1, today, buttons_required, buttons_text_splitted_array, id_counter));
                        // Open Modal
                        _this.checkModal(feedback_value);
                        chatColumn.classList.remove('loading');
                        if (_this.timer) {
                            clearTimeout(_this.timer);
                        }
                        _this.timer = setTimeout(function () {
                            // Displays response to top if response is bigger
                            msgDiv.scrollTop = previousscrollHeight - 10;
                            var messages = document.getElementById('scrollingChat').getElementsByClassName('clear');
                        }, 50);
                        /* document.getElementById('textInput').focus(); */
                    }, function (error) {
                        var serviceDownMsg = _this.langData.Log;
                        _this.segments.push(new dialog_response_1.DialogResponse(serviceDownMsg, false, ce, _this.langData.NResponse, today, buttons_required, buttons_text_splitted_array, id_counter));
                        chatColumn.classList.remove('loading');
                    });
                };
                AppComponent = __decorate([
                    core_1.Component({
                        directives: [ce_docs_1.CeDocComponent, payload_1.PayloadComponent, common_1.CORE_DIRECTIVES, common_1.FORM_DIRECTIVES],
                        providers: [dialog_service_1.DialogService],
                        selector: 'chat-app',
                        styles: ["\ninput:hover, \ninput:active,  \ninput:focus,\nbutton:focus,\nbutton:active,\nbutton:hover,\nlabel:focus,\n.btn:active,\n.btn.active\n{\n    outline:0px !important;\n    -webkit-appearance:none;\n    box-shadow: none;\n}\ninput[type='button']:disabled\n{\n    opacity:0.5;    \n}\ninput {\nfont-family: 'Open Sans', sans-serif;\nfont-size:10pt;\nfont-weight:normal;\n}\n.rosie {\nfont-family: \uFFFDRoboto\uFFFD, san-serif;\nfont-size: 10pt;\n}\n.respond {\nfont-family: 'Open Sans', sans-serif;\nfont-size:10pt;\ncolor: #444444;\nmargin-left: 10px;\n}\n.modal-backdrop1 {\n  position: fixed;\n  top: 0;\n  right: 0;\n  bottom: 0;\n  left: 0;\n  z-index: -1;\n  background-color: #000000;\n}\n.input-group-btn {\n    background-color: #FFFFFF !important;\n}\n"],
                        template: "\n <!-- \n  <div id='view-change-button' class='button' (click)='togglePanel($event)'>\n    <img title='Click to Collapse' class='option full' src='../img/Chat Button.png'>\n    <img title='Click to Expand' class='option not-full' src='../img/Code Button.png'>\n  </div>\n  -->\n<!--  -----------------------------------  Connecting Frankie  -----------------------------  -->\n<div class='success' style='margin-top: 35px;background-color: #F4F4F4;'>\n   <h3 class='w1' style='background-color: #F4F4F4;margin-top: -35px;\n      width: 372px;height: 460px;'>\n      <br><br>\n      <h4 class='' style='background-color: #F4F4F4;margin-top:190px;text-align:center;font-size:14pt;color: #009522;'>\n         Connecting to Frankie...\n      </h4>\n   </h3>\n</div>\n<!--  ----------------------------------- Connecting to Frankie -----------------------------  -->\n<div class='response' class='partition' id='parent' class='parentDiv' (window:resize)='onResize($event)' style='margin-top: -10px;'>\n   <div class='response' id='scrollingChat'>\n      <ng-container *ngFor=\"let segment of segments\">\n         <!--  ------------------------- Display User/Watson Bubbles ------------------------  -->\n         <div id='fwr' [class]='segment.isUser() ? \"from-user\" : (segment !== segments[segments.length - 1] ? \n            \"from-watson\" : \"from-watson-latest\")'>\n            <p *ngIf='!segment.isUser()' class='padding' [innerHtml]='segment.getText()'></p>\n            <!-- <p *ngIf='!segment.isUser()' style='font-size: 13px;margin-left: 200px;' class='padding' \n            [innerHtml]='segment.getTime()'></p> -->\n            <p *ngIf='segment.isUser()' class='padding' [innerHtml]='segment.getText()'></p><br>\n            <!-- <p *ngIf='segment.isUser()' style='float: right;font-size: 13px' class='padding' [innerHtml]='segment.getTime()'></p> -->\n            <!--  ------------------------- GetText ------------------------  -->\n            <div *ngIf='!segment.isUser() && segment.getCe().length>0 && segment !== segments[0]'>\n               <span title='Click to Collapse' (click)='CeToggle($event)' style='border : none;' class='expcoll'>\n               Collapse Results <span class='sign'>-</span></span>\n               <div class='toggleCe'>\n                  <ce-doc *ngFor='let doc of segment.getCe()' [doc]='doc'></ce-doc>\n               </div>\n            </div>\n         </div>\n         <!--  ------------------------- Display User/Watson Bubbles ------------------------  -->\n         <div class='clear'>\n         </div>\n         \n         <!--  ------------------------- Frankie Responding------------------------  -->\n         <div class='footer1' *ngIf='segment.isUser() && segment == segments[segments.length - 1]'>\n            <div style='background-color: #F4F4F4; width:287pt;' class='load1'>\n               <p class='respond'>Frankie is responding...</p>\n               <br><br>\n            </div>\n         </div>\n         <!--  ------------------------- Frankie Responding------------------------  -->\n         \n         <!--  ------------------------- Auto Buttons Checking ------------------------  -->\n         <div *ngFor=\"let button_Array of segment.getButtons_text_splitted_array();let j = index\" id='disable_button'>\n            <div *ngIf='!segment.isUser() && segment.getbuttons_required() > 0' [id]='segment.getid_counter()+j' \n            class='buttonAuto' (click)='button_click(button_Array,segment.getid_counter()+j)'>\n            {{button_Array}}\n         </div>\n   </div>\n   </ng-container>\n</div>\n<!--  ------------------------- Type question ------------------------  -->\n<div class='footer'>\n   <div class=\"input-group\" style=\"width: 100%;\">\n      <div class='inputOutline'>\n         <!--  ------------------------------------ Extra Button --------------------------------  -->\n         <div class=\"btn-group dropup\" style='width:11%;height: 40pt;float: left;' id=\"plus_button\">\n            <button type=\"button\" class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\" \n            style=\"border-color: white;padding: 19px 12px;\">\n            <span class=\"glyphicon glyphicon-plus\"></span>\n            </button>\n            <ul class=\"dropdown-menu\" role=\"menu\">\n               <li><a href=\"#\" id=\"new_chat\" (click)='reload(_dialogService)'><span class=\"glyphicon glyphicon-refresh\">\n               </span> Reload</a></li>\n               <li><a href=\"#\" (click)='sendMail()'><span class=\"glyphicon glyphicon-envelope\"></span> Email</a></li>\n               <li><a href=\"#\" (click)='print()'><span class=\"glyphicon glyphicon-print\"></span> Print</a></li>\n               <li class=\"divider\"></li>\n               <li id=\"li_tag\"><a href=\"#myModal\" data-toggle=\"modal\" data-target=\"#myModal\">\n                  <span class=\"glyphicon glyphicon-star\"></span> Feedback</a>\n               </li>\n            </ul>\n         </div>\n         <!--  ------------------------- Modal Design  ------------------------  -->\n         <div class=\"modal fade\" id=\"myModal\" role=\"dialog\">\n            <div class=\"modal-dialog\">\n               <div class=\"modal-content\">\n                  <div class='modal-content' style='margin-left: 35px;'>\n                     <div class='modal-body'>\n                        <form (ngSubmit)='onSubmit(carForm.value)' #carForm='ngForm'>   \n                        <div class='form-group row' style='margin-left: 52px;'>\n                           <label class='form-control-label col-sm-2' for='Rating' >Please Provide Ratings </label>\n                           <div class='col-sm-10 selectContainer'>\n                              <fieldset class=\"rating\">\n                                 <input type=\"radio\" id=\"star5\" name=\"rating\" value=\"5\" \n                                 [ngModel]=\"{checked: model.rating == '5 stars'}\" (ngModelChange)=\"model.rating='5 stars'\">\n                                 <label class = \"full\" for=\"star5\" \n                                    title=\"Awesome - 5 stars\"></label>\n                                 <input type=\"radio\" id=\"star4half\" name=\"rating\" value=\"4 and a half\" \n                                 [ngModel]=\"{checked: model.rating == '4.5 stars'}\" (ngModelChange)=\"model.rating='4.5 stars'\">\n                                 <label class=\"half\" for=\"star4half\" \n                                    title=\"Pretty good - 4.5 stars\"></label>\n                                 <input type=\"radio\" id=\"star4\" name=\"rating\" value=\"4\" \n                                 [ngModel]=\"{checked: model.rating == '4 stars'}\" (ngModelChange)=\"model.rating='4 stars'\">\n                                 <label class = \"full\" for=\"star4\" \n                                    title=\"Pretty good - 4 stars\"></label>\n                                 <input type=\"radio\" id=\"star3half\" name=\"rating\" value=\"3 and a half\" \n                                 [ngModel]=\"{checked: model.rating == '3.5 stars'}\" (ngModelChange)=\"model.rating='3.5 stars'\">\n                                 <label class=\"half\" for=\"star3half\" \n                                    title=\"Meh - 3.5 stars\"></label>\n                                 <input type=\"radio\" id=\"star3\" name=\"rating\" value=\"3\" \n                                 [ngModel]=\"{checked: model.rating == '3 stars'}\" (ngModelChange)=\"model.rating='3 stars'\">\n                                 <label class = \"full\" for=\"star3\" \n                                    title=\"Meh - 3 stars\"></label>\n                                 <input type=\"radio\" id=\"star2half\" name=\"rating\" value=\"2 and a half\" \n                                 [ngModel]=\"{checked: model.rating == '2.5 stars'}\" (ngModelChange)=\"model.rating='2.5 stars'\">\n                                 <label class=\"half\" for=\"star2half\" \n                                    title=\"Fair - 2.5 stars\"></label>\n                                 <input type=\"radio\" id=\"star2\" name=\"rating\" value=\"2\" \n                                 [ngModel]=\"{checked: model.rating == '2 stars'}\" (ngModelChange)=\"model.rating='2 stars'\">\n                                 <label class = \"full\" for=\"star2\" \n                                    title=\"Fair - 2 stars\"></label>\n                                 <input type=\"radio\" id=\"star1half\" name=\"rating\" value=\"1 and a half\" \n                                 [ngModel]=\"{checked: model.rating == '1.5 stars'}\" (ngModelChange)=\"model.rating='1.5 stars'\">\n                                 <label class=\"half\" \n                                    for=\"star1half\" title=\"Poor - 1.5 stars\"></label>\n                                 <input type=\"radio\" id=\"star1\" name=\"rating\" value=\"1\" \n                                 [ngModel]=\"{checked: model.rating == '1 stars'}\" (ngModelChange)=\"model.rating='1 stars'\">\n                                 <label class = \"full\" for=\"star1\" \n                                    title=\"Poor - 1 star\"></label>\n                                 <input type=\"radio\" id=\"starhalf\" name=\"rating\" value=\"half\" \n                                 [ngModel]=\"{checked: model.rating == '0.5 stars'}\" (ngModelChange)=\"model.rating='0.5 stars'\">\n                                 <label class=\"half\" for=\"starhalf\" \n                                    title=\"Sucks big time - 0.5 stars\"></label>\n                              </fieldset>\n                              <br><br>\n                              <div style='margin-left: 10px;'>You rated : {{model.rating}}</div>\n                              <!-- <div style=' margin-left: 15px;'>{{model | json}}</div>\n                                 <div><form ngForm action=\"\"></form></div> -->\n                           </div>\n                           <br>\n                        </div>\n                        <div class='form-group row'>\n                           <div class='col-sm-offset-2 col-sm-10'>\n                              <button type='submit' class='btn btn-success' style='margin-left: 100px;'>Submit</button>\n                           </div>\n                        </div>\n                        </form>\n                     </div>\n                  </div>\n               </div>\n            </div>\n         </div>\n         <!--  ------------------------- Modal Design  ------------------------  -->\n         <input  id='textInput' class='input responsive-column' type='text' class='form-control input-sm chat_input'\n         [(ngModel)]='question' name='question' (keydown)='keypressed($event)' placeholder='Type your question here...'\n         style='width:69%;height: 40pt;border: white;outline:none; box-shadow:none;' />   \n         <!--  ------------------------------------ Extra Button --------------------------------  -->\n         <!--  ------------------------------------ Send Button --------------------------------  -->\n         <input [disabled]='!question' type='button' class='input responsive-column' value=\"Send\"\n         class='btn btn-default btn-sm' id='btn-chat' style='font-weight: bold;height: 40pt; width: 20%;\n         border: white;color: #009522;'\n         (click)='keypressed($event)' />\n         <!--  ------------------------------------ Send Button --------------------------------  -->\n      </div>\n      <div class='draw'></div>\n   </div>\n</div>\n<!--  ------------------------- Type question ------------------------  -->\n</div>\n    "
                    }), 
                    __metadata('design:paramtypes', [dialog_service_1.DialogService, http_1.Http])
                ], AppComponent);
                return AppComponent;
            }());
            exports_1("AppComponent", AppComponent);
        }
    }
});

//# sourceMappingURL=app.component.js.map
