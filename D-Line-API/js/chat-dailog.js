/*   -----------------------------------------Adding CSS------------------------------- */
(function() {
    var css = [
		'../dist/css/dailog.css',
		'../dist/css/bootstrap.min.css',
		'https://fonts.googleapis.com/css?family=Roboto'
    ],
    i = 0,
    link = document.createElement('link'),
    head = document.getElementsByTagName('head')[0],
    tmp;
    link.rel = 'stylesheet';

    for(; i < css.length; i++){
        tmp = link.cloneNode(true);
        tmp.href = css[i];
        head.appendChild(tmp);
    }
})();
/*   -----------------------------------------End of Adding CSS------------------------------ */

/*   -----------------------------------------Adding Iframe-------------------------------  */
function load_index() {
	var isOpened = localStorage.getItem("isLoad");
	if( isOpened == null || isOpened == "" || isOpened != "true") {
		localStorage.setItem("isLoad","true");
		document.getElementById("myIframe").src = "https://webchat.botframework.com/embed/CG-HR-BOT-POC?s=LB8Pb6C_Ykg.cwA.ebo.pyTHy-QdA-l-yzHcmC9J-zJFEIuFbM4Bma4TA-by2Q8";
		
		ifr = document.getElementById("myIframe");
		ifr.style.width = '102.7%';
		ifr.style.height = '110%';
		ifr.style.marginTop = "-40px";
	}
	/*else {
		if (/iPad/.test(navigator.userAgent)) {
		var v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
		var ver = [parseInt(v[1], 10)];
		if (ver[0] >= 10) {
		jQuery(window.parent.document.getElementsByClassName('container_id')).removeAttr('style');
		jQuery(window.parent.document.getElementsByClassName('container_id')).css('width', '280pt');
		jQuery(window.parent.document.getElementsByClassName('container_id')).css('position', 'fixed');
		jQuery(window.parent.document.getElementsByClassName('container_id')).css('padding', '0px');
		jQuery(window.parent.document.getElementById('wrapper_ipad')).css('position', 'fixed');
		jQuery(window.parent.document.getElementById('wrapper_ipad')).css('bottom', '10px');
		jQuery(window.parent.document.getElementById('chatbotId')).removeAttr('style');
		jQuery(window.parent.document.getElementById('chatbotId')).css('position', 'fixed');
		jQuery(window.parent.document.getElementById('chatbotId')).css('bottom', '-22px');
		jQuery(window.parent.document.getElementById('chatbotId')).css('width', '280pt');
		jQuery(window.parent.document.getElementById('chatbotId')).css('padding', '0px');
		jQuery(window.parent.document.getElementById('chatbotId')).css('margin-bottom', '0');
		}
		}
	}*/
}
/*    -----------------------------------------Ending Iframe-------------------------------  */

/*   ------------------------------------------Adding Referrer------------------------------- */
/*function setReferrer() {
	localStorage.setItem("invokedFromIframe","true");
	console.log("Referrer set");
}*/
/*    -----------------------------------------Ending Referrer------------------------------ */

/*    ----------------------------------------Adding HTML------------------------------- */
var codeBlock = '<div class="container container_id" id="media" style="width: 280pt;padding: 0px;position:fixed">'+
'<span data-toggle="collapse" data-target="#collapseTwo"  href="#collapseTwo" class="collapsed icon" class="panel-heading" onClick="load_index()">'+
'<div class="row col-xs-5 col-md-3" id="chatbotId" style="bottom:-20px; position:fixed;width: 280pt !important;padding:0px">'+
'<div class="panel-group" id="accordion" style="width: 280pt !important;">'+
'<div class="panel panel-default chat-window-top-bar-container" style="width: 280pt !important">'+
'<div class="panel-heading"  style="background-color:#444443;height: 45pt;color:white;border-radius: 3pt 3pt 0 0;">'	+			
'<span data-toggle="collapse" data-target="#collapseTwo"  href="#collapseTwo" class="collapsed icon">' +
'<h4 class="panel-title" style="margin-top: 11px; margin-bottom: -27px;font-size: 14pt;"><span class="chat-window-icon"></span> Ask Frankie</h4></span>' +
'</div>'+
'<div id="collapseTwo" class="panel-collapse collapse">'+
'<div class="panel-body msg_container_base" style="margin: 0;padding: 0 0px;overflow:hidden; height : 355pt; width: 280pt; background-color: #F4F4F4;">'+
'<iframe id="myIframe"></iframe>'+
'</div>'+
'</div>'+
'</div>'+
'</div>'+
'</div>'+
'</span>'+
'</div>';			
$(document).ready(function(){
  $(".wrapper").html(codeBlock);
});
/*   --------------------------------------------Ending HTML--------------------------------------------------  */