/*   -----------------------------------------Adding CSS------------------------------- */
(function() {
    var css = [
        '../dist/css/style.css',
		'../dist/css/dailog.css',
		'../dist/font_awesome/css/font-awesome.min.css',
		'../dist/css/bootstrap.min.css'
    ],
    i = 0,
    link = document.createElement('link'),
    head = document.getElementsByTagName('head')[0],
    tmp;
    link.rel = 'stylesheet';

    for(; i < css.length; i++){
        tmp = link.cloneNode(true);
        tmp.href = css[i];
        head.appendChild(tmp);
    }
})();
/*   -----------------------------------------End of Adding CSS------------------------------ */

/*   -----------------------------------------Adding Iframe-------------------------------  */
window.setTimeout(function () {
   var iframe = document.getElementById('myIframe');
   iframe.setAttribute('src', 'index.html');
}, 5000);   
/*    -----------------------------------------Ending Iframe-------------------------------  */

/*   ------------------------------------------Adding Cookie------------------------------- */
window.onload = function() {
	localStorage.setItem("invokedFromIframe","true");
	console.log("Cookies set");
}
/*    -----------------------------------------Ending Cookie------------------------------ */

/*    ----------------------------------------Adding HTML------------------------------- */
var codeBlock = '<div class="container">'+
'<div class="row chat-window col-xs-5 col-md-3" id="chat_window_1" style="margin-right:700px;width:280pt;bottom:-20px; position:fixed;">'+
'<div class="col-xs-12 col-md-12">'+
'<div class="panel-group" id="accordion">'+
'<div class="panel panel-default" style="width: 275pt">'+
'<div class="panel-heading top-bar"  style="background-color:#444443;height: 45pt;color:white;padding :18px 1px 0px 0px;border-radius: 3pt 3pt 0 0;">'	+			
'<span class="glyphicon glyphicon-chevron-up icon_minim" style="color:white;width:370pt">'+
'<div class="col-md-8 col-xs-8">'+
'<h3 class="panel-title title" style="font-size: 14pt;"><span  class="fa fa-comments" style="width: 20pt;height: 20pt;"></span> Ask Rosie</h3>'+
'</div>'+
'<div class="col-md-4 col-xs-4" style="text-align: right;">'+
'<span><a href="#"></a></span>'+
'</div>'+
'</span>'+
'</div>'+
'<div id="collapseOne" class="panel-collapse collapse">'+
'<div class="panel-body msg_container_base" style="margin: 0;padding: 0 10px 10px;overflow:hidden; height : 300pt; width: 275pt; background-color: #F4F4F4;">'+
'<div id="hideMe"  style="background-color: #F4F4F4;margin-left: -20px;"><h3 class="w1" style="margin-top: -35px;margin-left: -20px;width: 340px;height: 430px;">'+
'<h4 style="margin-top: 190px; margin-left: 126px; font-size: 14pt;font-family: Open Sans Regular;color: #009522;">'+
'Connecting to Rosie...</h4></h3></div>'+
'<iframe id="myIframe" style=" margin-left: -12px; width: 284pt; height: 400pt; margin-top: -2px;background-color: #F4F4F4;"></iframe>'+
'</div>'+
'</div>'+
'</div>'+
'</div>'+
'</div>'+
'</div>'+
'</div>';			
$(document).ready(function(){
  $(".wrapper").html(codeBlock);
});
/*   --------------------------------------------Ending HTML--------------------------------------------------  */

/*   --------------------------------------------Code for UP/DOWN Arrow--------------------------------------  */

$(document).on('click', '.panel-heading span.icon_minim', function (e) {
  var $this = $(this);
  if (!$this.hasClass('panel-collapsed')) {
  $this.parents('.panel').find('#collapseOne').slideUp();
  $this.addClass('panel-collapsed');
  $this.removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');        
  } else {
  $this.parents('.panel').find('#collapseOne').slideDown();
  $this.removeClass('panel-collapsed');
  $this.removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
 }
});

/*    --------------------------------------------End for UP/DOWN Arrow------------------------------------  */