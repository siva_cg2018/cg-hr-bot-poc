System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var AppHelper;
    return {
        setters:[],
        execute: function() {
            AppHelper = (function () {
                function AppHelper(CarRegistrationNumber, PreviousInsurer, PolicyExpirationDate, AnyClaimsLastYear, FullName, Email, Mobile) {
                    this.CarRegistrationNumber = CarRegistrationNumber;
                    this.PreviousInsurer = PreviousInsurer;
                    this.PolicyExpirationDate = PolicyExpirationDate;
                    this.AnyClaimsLastYear = AnyClaimsLastYear;
                    this.FullName = FullName;
                    this.Email = Email;
                    this.Mobile = Mobile;
                }
                return AppHelper;
            }());
            exports_1("AppHelper", AppHelper);
        }
    }
});

//# sourceMappingURL=app.component.helper.js.map
