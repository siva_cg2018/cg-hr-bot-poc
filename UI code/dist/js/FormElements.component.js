System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var FormElements;
    return {
        setters:[],
        execute: function() {
            FormElements = (function () {
                function FormElements() {
                }
                FormElements.prototype.callElements = function (data) {
                    data = data.replace(/openingtagol/g, '<ol>');
                    data = data.replace(/closingtagol/g, '</ol>');
                    data = data.replace(/openingtagul/g, '<ul>');
                    data = data.replace(/closingtagul/g, '</ul>');
                    data = data.replace(/openingtagli/g, '<li>');
                    data = data.replace(/closingtagli/g, '</li>');
                    data = data.replace(/openingtagb/g, '<b>');
                    data = data.replace(/closingtagb/g, '</b>');
                    data = data.replace(/openingtaga/g, '<a ');
                    data = data.replace(/closingtaga/g, '</a>');
                    data = data.replace(/tagbr/g, '<br/>');
                    FormElements.element_value = data;
                    console.log(FormElements.element_value);
                };
                return FormElements;
            }());
            exports_1("FormElements", FormElements);
        }
    }
});

//# sourceMappingURL=FormElements.component.js.map
