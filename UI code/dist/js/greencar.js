System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var GreenCar;
    return {
        setters:[],
        execute: function() {
            GreenCar = (function () {
                function GreenCar(Rating, power, Comment) {
                    this.Rating = Rating;
                    this.power = power;
                    this.Comment = Comment;
                }
                return GreenCar;
            }());
            exports_1("GreenCar", GreenCar);
        }
    }
});

//# sourceMappingURL=greencar.js.map
