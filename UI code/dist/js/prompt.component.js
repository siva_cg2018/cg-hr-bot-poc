System.register(['@angular/core', 'ng2-bootstrap-modal'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __extends = (this && this.__extends) || function (d, b) {
        for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, ng2_bootstrap_modal_1;
    var PromptComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (ng2_bootstrap_modal_1_1) {
                ng2_bootstrap_modal_1 = ng2_bootstrap_modal_1_1;
            }],
        execute: function() {
            PromptComponent = (function (_super) {
                __extends(PromptComponent, _super);
                function PromptComponent(dialogService) {
                    _super.call(this, dialogService);
                    this.message = '';
                }
                PromptComponent.prototype.apply = function () {
                    this.result = this.message;
                    this.close();
                };
                PromptComponent = __decorate([
                    core_1.Component({
                        selector: 'prompt',
                        template: "<div class=\"modal-dialog\">\n                <div class=\"modal-content\">\n                   <div class=\"modal-header\">\n                     <button type=\"button\" class=\"close\" (click)=\"close()\">&times;</button>\n                     <h4 class=\"modal-title\">{{title || 'Prompt'}}</h4>\n                   </div>\n                   <div class=\"modal-body\">\n                    <label>{{question}}</label><input type=\"text\" class=\"form-control\" [(ngModel)]=\"message\" name=\"name\" >\n                   </div>\n                   <div class=\"modal-footer\">\n                     <button type=\"button\" class=\"btn btn-primary\" (click)=\"apply()\">OK</button>\n                     <button type=\"button\" class=\"btn btn-default\" (click)=\"close()\" >Cancel</button>\n                   </div>\n                 </div>\n                </div>"
                    }), 
                    __metadata('design:paramtypes', [(typeof (_a = typeof ng2_bootstrap_modal_1.DialogService !== 'undefined' && ng2_bootstrap_modal_1.DialogService) === 'function' && _a) || Object])
                ], PromptComponent);
                return PromptComponent;
                var _a;
            }(ng2_bootstrap_modal_1.DialogComponent));
            exports_1("PromptComponent", PromptComponent);
        }
    }
});

//# sourceMappingURL=prompt.component.js.map
