System.register(['./app.component'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var app_component_1;
    var PlusButton;
    return {
        setters:[
            function (app_component_1_1) {
                app_component_1 = app_component_1_1;
            }],
        execute: function() {
            PlusButton = (function () {
                function PlusButton() {
                }
                /**
                 * Opens email with proper chat history
                 *
                 * @param AppComponent.segment_data :  Array of Response
                 * AppComponent is a parent component
                 */
                PlusButton.prototype.sendMail = function () {
                    var iframe = '';
                    for (var i = 0; i < app_component_1.AppComponent.segment_data.length; i++) {
                        var splitted = app_component_1.AppComponent.segment_data[i].getTime().split(',');
                        if (app_component_1.AppComponent.segment_data[i].isUser()) {
                            iframe = iframe + '%5B' + splitted[1] + '%20at%20' + splitted[0] + '%20%5D' + '%20%20' + 'You%20:%20%20' +
                                app_component_1.AppComponent.segment_data[i].getText() + '%0D%0A';
                        }
                        else {
                            iframe = iframe + '%5B' + splitted[1] + '%20at%20' + splitted[0] + '%20%5D' + '%20%20' + 'Bot%20:%20%20' +
                                app_component_1.AppComponent.segment_data[i].getText() + '%0D%0A';
                        }
                    }
                    var link = 'mailto:email@domain.com?subject=Chatbot Backup' +
                        '&body=Hi%20,%0D%0A%0D%0APlease%20find%20your%20chat%20transcript%20as%20below.%0D%0A%0D%0A' +
                        iframe + '%0D%0A%0D%0AThanks%20%26%20Regards%2C%0D%0A%0D%0AAGH%20Team.';
                    window.top.location.href = link;
                };
                /**
                 * Prints chat history
                 */
                PlusButton.prototype.print = function () {
                    window.print();
                };
                return PlusButton;
            }());
            exports_1("PlusButton", PlusButton);
        }
    }
});

//# sourceMappingURL=PlusButton.component.js.map
