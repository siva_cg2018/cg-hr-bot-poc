System.register(['./PlusButton.component', 'angular2/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __param = (this && this.__param) || function (paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    };
    var PlusButton_component_1, core_1;
    var ExternalMethods;
    return {
        setters:[
            function (PlusButton_component_1_1) {
                PlusButton_component_1 = PlusButton_component_1_1;
            },
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            ExternalMethods = (function () {
                function ExternalMethods(_plusButton) {
                    this._plusButton = _plusButton;
                    console.log(_plusButton + 'PlusButton');
                }
                /**
                 * This method checks which button is clicked using method_id as parameter
                 */
                ExternalMethods.prototype.callMethod = function (method_id) {
                    if (method_id === 'print') {
                        this._plusButton.print();
                    }
                    if (method_id === 'sendMail') {
                        this._plusButton.sendMail();
                    }
                };
                ExternalMethods = __decorate([
                    __param(0, core_1.Inject(PlusButton_component_1.PlusButton)), 
                    __metadata('design:paramtypes', [PlusButton_component_1.PlusButton])
                ], ExternalMethods);
                return ExternalMethods;
            }());
            exports_1("ExternalMethods", ExternalMethods);
        }
    }
});

//# sourceMappingURL=ExternalMethods.component.js.map
