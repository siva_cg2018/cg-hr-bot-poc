System.register(['angular2/core', 'angular2/common', './greencar'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, common_1, greencar_1;
    var FormExample;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (greencar_1_1) {
                greencar_1 = greencar_1_1;
            }],
        execute: function() {
            // Our form component
            FormExample = (function () {
                function FormExample() {
                    this.myCar = new greencar_1.GreenCar(1, 'BMW Serie 1', 'Red', 2);
                    this.submitted = false;
                }
                FormExample.prototype.onSubmit = function (data) {
                    this.submitted = true;
                    this.data = JSON.stringify(data, null, 2);
                    console.log(this.data);
                };
                FormExample = __decorate([
                    core_1.Component({
                        directives: [common_1.CORE_DIRECTIVES, common_1.FORM_DIRECTIVES],
                        selector: 'angular-typescript-form',
                        templateUrl: 'app/angular-typescript-form.component.html',
                    }), 
                    __metadata('design:paramtypes', [])
                ], FormExample);
                return FormExample;
            }());
            exports_1("FormExample", FormExample);
        }
    }
});

//# sourceMappingURL=angular-typescript-form.component.js.map
