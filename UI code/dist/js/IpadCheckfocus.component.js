System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var IpadCheckfocus;
    return {
        setters:[],
        execute: function() {
            IpadCheckfocus = (function () {
                function IpadCheckfocus() {
                }
                IpadCheckfocus.prototype.checkFocus = function () {
                    var landscape = jQuery(window.parent.document.getElementById('media')).css('margin-left');
                    if (/iPad/.test(navigator.userAgent) && landscape === '450px') {
                        var v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
                        var ver = [parseInt(v[1], 10)];
                        if (ver[0] >= 10) {
                            jQuery(window.parent.document.getElementsByClassName('container_id')).removeAttr('style');
                            jQuery(window.parent.document.getElementsByClassName('container_id')).css('width', '280pt');
                            jQuery(window.parent.document.getElementsByClassName('container_id')).css('padding', '0px');
                            jQuery(window.parent.document.getElementById('wrapper_ipad')).css('position', 'absolute');
                            jQuery(window.parent.document.getElementById('wrapper_ipad')).css('bottom', '10px');
                            jQuery(window.parent.document.getElementById('chatbotId')).removeAttr('style');
                            jQuery(window.parent.document.getElementById('chatbotId')).css('position', 'relative');
                            jQuery(window.parent.document.getElementById('chatbotId')).css('bottom', '-167px');
                            jQuery(window.parent.document.getElementById('chatbotId')).css('width', '280pt');
                            jQuery(window.parent.document.getElementById('chatbotId')).css('padding', '0px');
                            jQuery(window.parent.document.getElementById('chatbotId')).css('margin-bottom', '-20px');
                        }
                    }
                };
                IpadCheckfocus.prototype.lost_checkFocus = function () {
                    var landscape = jQuery(window.parent.document.getElementById('media')).css('margin-left');
                    if (/iPad/.test(navigator.userAgent) && landscape === '450px') {
                        var v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
                        var ver = [parseInt(v[1], 10)];
                        if (ver[0] >= 10) {
                            jQuery(window.parent.document.getElementsByClassName('container_id')).removeAttr('style');
                            jQuery(window.parent.document.getElementsByClassName('container_id')).css('width', '280pt');
                            jQuery(window.parent.document.getElementsByClassName('container_id')).css('position', 'fixed');
                            jQuery(window.parent.document.getElementsByClassName('container_id')).css('padding', '0px');
                            jQuery(window.parent.document.getElementById('wrapper_ipad')).removeAttr('style');
                            jQuery(window.parent.document.getElementById('chatbotId')).removeAttr('style');
                            jQuery(window.parent.document.getElementById('chatbotId')).css('position', 'fixed');
                            jQuery(window.parent.document.getElementById('chatbotId')).css('bottom', '-20px');
                            jQuery(window.parent.document.getElementById('chatbotId')).css('width', '280pt');
                            jQuery(window.parent.document.getElementById('chatbotId')).css('padding', '0px');
                        }
                    }
                };
                return IpadCheckfocus;
            }());
            exports_1("IpadCheckfocus", IpadCheckfocus);
        }
    }
});

//# sourceMappingURL=IpadCheckfocus.component.js.map
