System.register(['rxjs/add/operator/toPromise', './dialog.response', './dialog.service', 'angular2/http', './ce.docs', './payload', 'angular2/core', 'angular2/common', './PlusButton.component', './ExternalMethods.component'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var dialog_response_1, dialog_service_1, http_1, ce_docs_1, payload_1, core_1, common_1, PlusButton_component_1, ExternalMethods_component_1;
    var AppComponent;
    return {
        setters:[
            function (_1) {},
            function (dialog_response_1_1) {
                dialog_response_1 = dialog_response_1_1;
            },
            function (dialog_service_1_1) {
                dialog_service_1 = dialog_service_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (ce_docs_1_1) {
                ce_docs_1 = ce_docs_1_1;
            },
            function (payload_1_1) {
                payload_1 = payload_1_1;
            },
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (PlusButton_component_1_1) {
                PlusButton_component_1 = PlusButton_component_1_1;
            },
            function (ExternalMethods_component_1_1) {
                ExternalMethods_component_1 = ExternalMethods_component_1_1;
            }],
        execute: function() {
            /*
             * Main entry point to the application. This component is responsible for the entire page layout.
             */
            AppComponent = (function () {
                function AppComponent(_dialogService, http, _plusButton, _externalMethods) {
                    this._dialogService = _dialogService;
                    this.http = http;
                    this._plusButton = _plusButton;
                    this._externalMethods = _externalMethods;
                    this.response = null;
                    this.buttons_text_splitted_array = [];
                    this.timer = null;
                    this.setupTimer = null;
                    this.setupTimer1 = null;
                    this.question = null;
                    this.segments = []; // Array of requests and responses
                    this.workspace_id = null;
                    this.submitted = false;
                    this.getLang();
                    this.model = {
                        rating: '0 star'
                    };
                }
                /*
                * This method is responsible for making a request to Conversation service with the corresponding user query.
                */
                AppComponent.prototype.callConversationService = function (chatColumn, payload) {
                    var _this = this;
                    var responseText = '';
                    var ce = null;
                    var language_preffered;
                    var buttons_required;
                    var buttons_text_splitted_array = null;
                    var buttons_text_counter = null;
                    // Get timestamp for every response
                    var d = new Date();
                    var feedback_value = false;
                    var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July',
                        'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
                    ];
                    var id_counter = ('0' + d.getMinutes()).slice(-2) + ('0' + d.getSeconds()).slice(-2);
                    var today = ('0' + d.getHours()).slice(-2) + ':' + ('0' + d.getMinutes()).slice(-2) +
                        ',' + ' ' + monthNames[d.getMonth()] + ' ' + d.getDate();
                    // Before calling conversation, set the call_retrieve_and_rank flag to false
                    // conversation and not the app should control when retrieve and rank is called
                    if (payload.context) {
                        payload.context.call_retrieve_and_rank = false;
                    }
                    // ..........Start Reload Functionality..........
                    if (payload.input.text === '') {
                        this.segments.length = 0;
                        jQuery('.success').show();
                        setTimeout(function () {
                            jQuery('.success').hide();
                        }, 3000);
                    }
                    // ..........End Reload Functionality..........
                    // Send the user utterance to dialog, also send previous context
                    this._dialogService.message(this.workspace_id, payload).subscribe(function (data1) {
                        _this.response = data1;
                        if (data1) {
                            if (data1.error) {
                                responseText = data1.error;
                                data1 = _this.langData.NResponse;
                            }
                            else if (data1.output) {
                                if (data1.output.CEPayload && data1.output.CEPayload.length > 0) {
                                    ce = data1.output.CEPayload;
                                    responseText = _this.langData.Great;
                                }
                                else if (data1.output.text) {
                                    responseText = data1.output.text.length >= 1 && !data1.output.text[0] ? data1.output.text.join(' ').trim() : data1.output.text[0]; // tslint:disable-line max-line-length
                                }
                            }
                        }
                        language_preffered = _this.response.context.language_preffered;
                        if (language_preffered !== null && language_preffered !== undefined) {
                            if (language_preffered === 'English') {
                                _this.workspace_id = '546939fe-2ce5-47c5-b4df-7924cfe5a0e6';
                            }
                            if (language_preffered === 'Arabic') {
                                _this.workspace_id = 'ef6fa340-903b-41b3-b2a7-652fd238edfa';
                            }
                        }
                        buttons_required = _this.response.context.buttons_required;
                        var buttons_text = _this.response.context.buttonsText;
                        if (buttons_text != null) {
                            buttons_text_splitted_array = buttons_text.split(',');
                            _this.response.context.buttons_required = null;
                            _this.response.context.buttonsText = null;
                        }
                        for (var i = 0; i < buttons_required; i++) {
                            var autoButton = document.createElement('button');
                            jQuery('#' + id_counter).append(autoButton);
                            jQuery('.typeahead').val('');
                            jQuery('.typeahead').attr('disabled', 'disabled');
                        }
                        // Check feedback value true or false
                        feedback_value = _this.response.context.feedback;
                        var msgDiv = document.getElementById('scrollingChat');
                        var previousscrollHeight = msgDiv.scrollHeight;
                        _this.segments.push(new dialog_response_1.DialogResponse(responseText, false, ce, data1, today, buttons_required, buttons_text_splitted_array, id_counter));
                        // Open Modal
                        _this.checkModal(feedback_value);
                        chatColumn.classList.remove('loading');
                        AppComponent.segment_data = _this.segments;
                        if (_this.timer) {
                            clearTimeout(_this.timer);
                        }
                        _this.timer = setTimeout(function () {
                            // Displays response to top if response is bigger
                            msgDiv.scrollTop = previousscrollHeight - 10;
                            var messages = document.getElementById('scrollingChat').getElementsByClassName('clear');
                        }, 50);
                        /* document.getElementById('textInput').focus(); */
                    }, function (error) {
                        var serviceDownMsg = _this.langData.Log;
                        _this.segments.push(new dialog_response_1.DialogResponse(serviceDownMsg, false, ce, _this.langData.NResponse, today, buttons_required, buttons_text_splitted_array, id_counter));
                        chatColumn.classList.remove('loading');
                    });
                };
                AppComponent.prototype.child_method = function (method_id) {
                    this._externalMethods.callMethod(method_id);
                };
                /*
                 * This method is responsible for triggering a request whenever a Enter key is pressed .  || event.which === 1
                 */
                AppComponent.prototype.keypressed = function (event) {
                    var element = document.querySelector('.draw');
                    var nw = element.offsetWidth + 7;
                    if (event && event.keyCode === 8) {
                        if (this.question === null || this.question === '') {
                            return false;
                        }
                        else {
                            nw = element.offsetWidth - 7;
                        }
                    }
                    if (nw > 360) {
                        nw = 360;
                    }
                    if (AppComponent.typeahead_data != null && AppComponent.typeahead_data !== '') {
                        this.question = AppComponent.typeahead_data;
                    }
                    element.style.width = String(nw + 'px');
                    // On enter sendData/On button click
                    if (event && event.keyCode === 13 || event.which === 1) {
                        // To check input tag and dont submit if null
                        if (this.question === null || this.question === '') {
                            return false;
                        }
                        else {
                            this.sendData();
                            element.style.width = '0px';
                        }
                        event.preventDefault();
                        AppComponent.typeahead_data = null;
                    }
                };
                AppComponent.prototype.ngOnInit = function () {
                    var typeahead_var = null;
                    var bloodhoundSuggestions = new Bloodhound({
                        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
                        prefetch: {
                            cache: false,
                            ttl: 100000,
                            url: '/rest/listExamples'
                        },
                        queryTokenizer: Bloodhound.tokenizers.whitespace,
                        sufficient: 3
                    });
                    bloodhoundSuggestions.clearRemoteCache();
                    bloodhoundSuggestions.clear();
                    bloodhoundSuggestions.clearPrefetchCache();
                    bloodhoundSuggestions.initialize(true);
                    jQuery('#textInput .typeahead').typeahead({
                        highlight: true,
                        hint: false,
                        minLength: 1
                    }, {
                        displayKey: 'value',
                        name: 'suggestions',
                        source: bloodhoundSuggestions
                    }).on('typeahead:selected', function (e, data) {
                        console.log(data.value);
                        AppComponent.typeahead_data = data.value;
                        jQuery('#btn-chat').removeAttr('disabled');
                        jQuery('.typeahead').empty();
                        jQuery('.typeahead').val('');
                    }).on('keyup', function (e) {
                        if (e.which === 13) {
                            jQuery('.tt-suggestion:first-child', this);
                            console.log(this.value + 'Typehead keyup');
                            AppComponent.typeahead_data = this.value;
                        }
                    });
                };
                /*-------------------------------------------------------- Submit Feedback Value --------------------------------*/
                AppComponent.prototype.onSubmit = function (data) {
                    this.submitted = true;
                    var chatColumn = document.querySelector('#scrollingChat');
                    chatColumn.classList.add('loading');
                    this.model = JSON.stringify(this.model);
                    var context = null;
                    if (this.response != null) {
                        context = this.response.context;
                        // we are going to delete the context variable 'callRetrieveAndRank' before
                        // sending back to the Conversation service
                        if (context && context.callRetrieveAndRank) {
                            delete context.callRetrieveAndRank;
                        }
                    }
                    var inputString = this.model;
                    /* --------------------- Split Rating string --------------------------  */
                    var inputString_split = inputString.split('"');
                    var input = {
                        'text': inputString_split[1] + '-' + inputString_split[3]
                    };
                    /*  --------------------- Split Rating string --------------------------  */
                    var payloaddata = {
                        input: input,
                        context: context
                    };
                    this.callConversationService(chatColumn, payloaddata);
                    /* Hides modal after submit */
                    jQuery('#myModal').modal('hide');
                    jQuery('#li_tag').attr('disabled', 'true');
                    jQuery('.typeahead').val('');
                    /* Hide Modal only one after submit
                    jQuery('.modal').removeClass('in');
                    jQuery('.modal').attr('aria-hidden', 'true');
                    jQuery('.modal').css('display', 'none');
                    jQuery('.modal-backdrop').remove();
                    jQuery('body').removeClass('modal-open');
                    return false; */
                };
                /*
                 * This method is responsible for detecting user locale and getting locale specific content to be displayed by making a
                 * GET request to the respective file.
                 */
                AppComponent.prototype.getLang = function () {
                    var _this = this;
                    var browserLang = window.navigator.language || window.navigator.userLanguage;
                    var complLang = browserLang.split('-');
                    var time;
                    var id_counter;
                    var buttons_required;
                    var buttons_text_splitted_array;
                    var lang = complLang[0];
                    var lang_url = 'locale/' + lang + '.json';
                    this.http.get(lang_url).map(function (res) { return res.json(); }).subscribe(function (data) {
                        _this.langData = data;
                    }, function (error) {
                        var lang_url = 'locale/en.json';
                        _this.http.get(lang_url).map(function (res) { return res.json(); }).subscribe(function (data) {
                            _this.langData = data;
                            _this.segments.push(new dialog_response_1.DialogResponse(_this.langData.Description, false, null, null, time, buttons_required, buttons_text_splitted_array, id_counter));
                        }, function (error) { return alert(JSON.stringify(error)); });
                    });
                };
                AppComponent.prototype.ngAfterViewInit = function (_dialogService) {
                    this.checkSetup(_dialogService);
                };
                /*
                 * This method is responsible for detecting if the set-up processs involving creation of various Watson services
                 * and configuring them is complete. The status is checked every one minute till its complete.
                 * A loading screen is displayed to show set-up progress accordingly.
                 */
                AppComponent.prototype.checkSetup = function (_dialogService) {
                    var _this = this;
                    this._dialogService.setup().subscribe(function (data) {
                        _this.workspace_id = data.WORKSPACE_ID;
                        console.log(data.UTTERANCES);
                        var setup_state = data.setup_state;
                        var setup_status_msg = data.setup_status_message;
                        var setup_phase = data.setup_phase;
                        var setup_message = data.setup_message;
                        var setup_step = data.setup_step;
                        var setup = document.querySelector('.setup');
                        var setup_status = document.querySelector('.setup-msg');
                        var chat_app = document.querySelector('chat-app');
                        var setupLoader = document.querySelector('.setup-loader');
                        var setupPhase = document.querySelector('.setup-phase');
                        var setupPhaseMsg = document.querySelector('.setup-phase-msg');
                        var errorPhase = document.querySelector('.error-phase');
                        var errorPhaseMsg = document.querySelector('.error-phase-msg');
                        var circles = document.querySelector('.circles');
                        var gerror = document.querySelector('.gerror');
                        var werror = document.querySelector('.werror');
                        var activeCircle = document.querySelector('.active-circle');
                        var nactiveCircle = document.querySelector('.non-active-circle');
                        setup_status.innerHTML = setup_status_msg;
                        if (setup_state === 'not_ready') {
                            document.body.style.backgroundColor = 'darkgray';
                            chat_app.style.opacity = '0.25';
                            setup.style.display = 'block';
                            setupPhase.innerHTML = setup_phase;
                            setupPhaseMsg.innerHTML = setup_message;
                            if (setup_step === '0') {
                                errorPhase.innerHTML = setup_phase;
                                errorPhaseMsg.innerHTML = setup_message;
                                setupLoader.style.display = 'none';
                                setupPhase.style.display = 'none';
                                setupPhaseMsg.style.display = 'none';
                                circles.style.display = 'none';
                                if (setup_phase !== 'Error') {
                                    werror.style.display = 'block';
                                }
                                else {
                                    gerror.style.display = 'block';
                                }
                                errorPhase.style.display = 'block';
                                errorPhaseMsg.style.display = 'block';
                            }
                            else {
                                setupLoader.style.display = 'block';
                                setupPhase.style.display = 'block';
                                setupPhaseMsg.style.display = 'block';
                                circles.style.display = 'block';
                                gerror.style.display = 'none';
                                werror.style.display = 'none';
                                errorPhase.style.display = 'none';
                                errorPhaseMsg.style.display = 'none';
                            }
                            if (setup_step === '2') {
                                activeCircle.classList.remove('active-circle');
                                activeCircle.classList.add('non-active-circle');
                                nactiveCircle.classList.remove('non-active-circle');
                                nactiveCircle.classList.add('active-circle');
                            }
                            _this.setupTimer = setTimeout(function () {
                                _this.checkSetup(_dialogService);
                            }, 60000);
                        }
                        else {
                            var payload = { 'input': { 'text': '' } };
                            var chatColumn = document.querySelector('#scrollingChat');
                            _this.callConversationService(chatColumn, payload);
                            // Replaced bg color with image
                            // document.body.style.backgroundColor = 'white';
                            chat_app.style.opacity = '1';
                            setup.style.display = 'none';
                            if (_this.setupTimer) {
                                clearTimeout(_this.setupTimer);
                            }
                        }
                    }, function (error) { return alert(JSON.stringify(error)); });
                    // this._dialogService.callTypeaheadService();
                };
                /*
                 * This method is responsible for toggling Expand/Collapse section of CE content.
                 */
                AppComponent.prototype.CeToggle = function (event) {
                    var targetElement;
                    if (event.srcElement) {
                        targetElement = event.srcElement;
                    }
                    else {
                        targetElement = event.target;
                    }
                    if (targetElement.className === 'sign') {
                        targetElement = targetElement.parentElement;
                    }
                    if (targetElement.innerText.indexOf('Collapse') !== -1) {
                        targetElement.innerHTML = this.langData.EResults + '<span class=sign>+</span>';
                        targetElement.style.border = '';
                        targetElement.title = this.langData.Expand;
                    }
                    else {
                        targetElement.innerHTML = this.langData.CResults + '<span class=sign>-</span>';
                        targetElement.style.border = 'none';
                        targetElement.title = this.langData.Collapse;
                    }
                    var expcoll = targetElement.nextElementSibling;
                    if (expcoll && (expcoll.style.display === 'block' || expcoll.style.display === '')) {
                        expcoll.style.display = 'none';
                    }
                    else {
                        expcoll.style.display = 'block';
                    }
                };
                /*
                 * This method is responsible for preparing the data to send and call the method for Conversation Service
                 */
                AppComponent.prototype.sendData = function () {
                    var chatColumn = document.querySelector('#scrollingChat');
                    chatColumn.classList.add('loading');
                    // Get timestamp for every response
                    var d = new Date();
                    var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July',
                        'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
                    ];
                    var id_counter = ('0' + d.getMinutes()).slice(-2) + ('0' + d.getSeconds()).slice(-2);
                    var today = ('0' + d.getHours()).slice(-2) + ':' + ('0' + d.getMinutes()).slice(-2) +
                        ',' + ' ' + monthNames[d.getMonth()] + ' ' + d.getDate();
                    var q = '';
                    var buttons_required;
                    var buttons_text_splitted_array;
                    if (this.question != null) {
                        q = this.question;
                    }
                    this.question = null;
                    var context = null;
                    if (this.response != null) {
                        context = this.response.context;
                        // we are going to delete the context variable 'callRetrieveAndRank' before
                        // sending back to the Conversation service
                        if (context && context.callRetrieveAndRank) {
                            delete context.callRetrieveAndRank;
                        }
                    }
                    var input = {
                        'text': q
                    };
                    var payload = {
                        input: input,
                        context: context
                    };
                    var time;
                    // Add the user utterance to the list of chat segments
                    this.segments.push(new dialog_response_1.DialogResponse(q, true, null, payload, today, buttons_required, buttons_text_splitted_array, id_counter));
                    this.timer = setTimeout(function () {
                        var msgDiv = document.getElementById('scrollingChat');
                        // Display Message before watson response
                        msgDiv.scrollTop = msgDiv.scrollHeight;
                        var messages = document.getElementById('scrollingChat').getElementsByClassName('clear');
                    }, 50);
                    // Call the method which calls the proxy for the message api
                    this.callConversationService(chatColumn, payload);
                    jQuery('#ta').val('');
                };
                /*--------------------------------------Checks Value Before modal Open-------------------------------*/
                AppComponent.prototype.checkModal = function (responseText) {
                    if (responseText === true) {
                        this.submitted = false;
                        this.model = {
                            rating: '0 star'
                        };
                        jQuery('#myModal').modal('show');
                    }
                };
                /*-------------------------------------- Checks Button Value -------------------------------*/
                AppComponent.prototype.button_click = function (data, button_id) {
                    jQuery('#' + button_id).css({
                        'background': '#002080',
                        'color': 'white'
                    });
                    var questionData = true;
                    if (data.trim() === 'English' || data.trim() === 'Arabic') {
                        this.question = 'I want to chat in ' + data;
                        questionData = false;
                    }
                    if (questionData) {
                        this.question = data;
                    }
                    this.keypressed(event);
                    jQuery('.typeahead').removeAttr('disabled');
                    /* jQuery('.typeahead').focus(); */
                    AppComponent.typeahead_data = null;
                    jQuery('.typeahead').val('');
                    jQuery('#disable_button').attr('disabled', 'disabled');
                };
                AppComponent.typeahead_data = null;
                AppComponent = __decorate([
                    core_1.Component({
                        directives: [ce_docs_1.CeDocComponent, payload_1.PayloadComponent, common_1.CORE_DIRECTIVES, common_1.FORM_DIRECTIVES],
                        providers: [dialog_service_1.DialogService, PlusButton_component_1.PlusButton, ExternalMethods_component_1.ExternalMethods],
                        selector: 'chat-app',
                        styles: ["\ninput:hover, \ninput:active,  \ninput:focus,\nbutton:focus,\nbutton:active,\nbutton:hover,\nlabel:focus,\n.btn:active,\n.btn.active\n{\n    outline:0px !important;\n    -webkit-appearance:none;\n    box-shadow: none;\n}\ninput[type='button']:disabled\n{\n    opacity:0.5;    \n}\ninput {\nfont-family: 'Open Sans', sans-serif;\nfont-size:10pt;\nfont-weight:normal;\n}\n.rosie {\nfont-family: \uFFFDRoboto\uFFFD, san-serif;\nfont-size: 10pt;\n}\n.respond {\nfont-family: 'Open Sans', sans-serif;\nfont-size:10pt;\ncolor: #444444;\nmargin-left: 10px;\n}\n.modal-backdrop1 {\n  position: fixed;\n  top: 0;\n  right: 0;\n  bottom: 0;\n  left: 0;\n  z-index: -1;\n  background-color: #000000;\n}\n.input-group-btn {\n    background-color: #FFFFFF !important;\n}\n"],
                        templateUrl: './app.component.html'
                    }), 
                    __metadata('design:paramtypes', [dialog_service_1.DialogService, http_1.Http, PlusButton_component_1.PlusButton, ExternalMethods_component_1.ExternalMethods])
                ], AppComponent);
                return AppComponent;
            }());
            exports_1("AppComponent", AppComponent);
        }
    }
});

//# sourceMappingURL=app.component.js.map
