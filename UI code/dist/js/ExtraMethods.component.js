System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var ExtraMethods;
    return {
        setters:[],
        execute: function() {
            ExtraMethods = (function () {
                function ExtraMethods() {
                }
                /*
                 * This method is responsible for changing the layout of payload section based on screen resolution.
                 */
                ExtraMethods.prototype.resizePayloadColumn = function (rightColumn) {
                    if (window.innerWidth < 730) {
                        rightColumn.classList.add('no-show');
                    }
                    else if (window.innerWidth < 830) {
                        rightColumn.classList.remove('no-show');
                        rightColumn.style.width = '340px';
                    }
                    else if (window.innerWidth < 860) {
                        rightColumn.classList.remove('no-show');
                        rightColumn.style.width = '445px';
                    }
                    else if (window.innerWidth < 951) {
                        rightColumn.classList.remove('no-show');
                        rightColumn.style.width = '395px';
                    }
                    else {
                        rightColumn.classList.remove('no-show');
                        rightColumn.style.width = '445px';
                    }
                };
                ExtraMethods.prototype.onResize = function (event) {
                    // let rightColumn = <HTMLElement>document.querySelector ('.right');
                    // this.resizePayloadColumn (rightColumn);
                };
                /*
                 * This method is responsible for toggling the payload section to full screen or fixed layout by
                 * clicking the Code Expand/Collapse icons at the top right .
                 */
                ExtraMethods.prototype.togglePanel = function (event) {
                    /* let payloadColumn = <HTMLElement>document.querySelector ('#payload-column');
                    let toggleButton = <HTMLElement>document.querySelector ('#view-change-button');
                    let rightColumn = <HTMLElement>document.querySelector ('.right');
                    let element;
                    if (event.srcElement) {
                      element = event.srcElement;
                    } else {
                      element = event.target;
                    }
                    if (toggleButton.classList.contains ('full')) {
                      toggleButton.classList.remove ('full');
                      payloadColumn.classList.remove ('full');
                      this.resizePayloadColumn (rightColumn);
                    } else {
                      rightColumn.classList.remove ('no-show');
                      rightColumn.style.width = '100%';
                      toggleButton.classList.add ('full');
                      payloadColumn.classList.add ('full');
                    }
                     */
                };
                return ExtraMethods;
            }());
            exports_1("ExtraMethods", ExtraMethods);
        }
    }
});

//# sourceMappingURL=ExtraMethods.component.js.map
