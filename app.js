	/*-----------------------------------------------------------------------------
	A simple echo bot for the Microsoft Bot Framework. 
	-----------------------------------------------------------------------------*/

	var restify = require('restify');
	var builder = require('botbuilder');
	var botbuilder_azure = require("botbuilder-azure");
	var fs = require("fs");

	const helpers = require("./lib/helper.js")

	// Setup Restify Server
	var server = restify.createServer();
	server.listen(process.env.port || process.env.PORT || 3978, function () {
	   console.log('%s listening to %s', server.name, server.url); 
	});
	  
	// Create chat connector for communicating with the Bot Framework Service
	var connector = new builder.ChatConnector({
	    appId: process.env.MicrosoftAppId,
	    appPassword: process.env.MicrosoftAppPassword,
	    openIdMetadata: process.env.BotOpenIdMetadata 
	});

	// Listen for messages from users 
	server.post('/api/messages', connector.listen());

	/*----------------------------------------------------------------------------------------
	* Bot Storage: This is a great spot to register the private state storage for your bot. 
	* We provide adapters for Azure Table, CosmosDb, SQL Azure, or you can implement your own!
	* For samples and documentation, see: https://github.com/Microsoft/BotBuilder-Azure
	* ---------------------------------------------------------------------------------------- */

	var tableName = 'botdata';

	var azureTableClient = new botbuilder_azure.AzureTableClient(tableName, process.env['AzureWebJobsStorage']);
	var tableStorage = new botbuilder_azure.AzureBotStorage({ gzipData: false }, azureTableClient);


	var response = fs.readFileSync("data/response.json","utf8");
	response = JSON.parse(response);

	// Create your bot with a function to receive messages from the user
	var bot = new builder.UniversalBot(connector/*,[
		function(session){
			greetPreefix = helpers.greetPreefix();
			message = response["greetings"].replace("{greet}",greetPreefix);
			session.send(message);
		}
		]*/);
		//bot.set('storage', tableStorage);

	// Make sure you add code to validate these fields
	var luisAppId = process.env.LuisAppId || "6ad63985-7c89-4603-8930-4e5fbfbb9d76";
	var luisAPIKey = "832c698bc6a143e2b963ccb0f9064b14" ;//process.env.LuisAPIKey;
	var luisAPIHostName = process.env.LuisAPIHostName || 'westus.api.cognitive.microsoft.com';

	const LuisModelUrl = 'https://' + luisAPIHostName + '/luis/v1/application?id=' + luisAppId + '&subscription-key=' + luisAPIKey;
	console.log("LuisModelUrl : "+LuisModelUrl);
	// Main dialog with LUIS
	var recognizer = new builder.LuisRecognizer(LuisModelUrl);

	var intents = new builder.IntentDialog({ recognizers: [recognizer] })

	.matches('Greeting', (session, args) => {

	    //session.send('You reached Greeting intent, you said \'%s\'.', session.message.text);
	    greetPreefix = helpers.greetPreefix();
	    var text = response["greetings"]["text"];
		text = text.replace("{greet}",greetPreefix);
		buttons = response["greetings"]["buttons"]; //[{"label" : "Onboard", "print" : "Onboard"}, {"label" : "Leave Policy" , "print" : "Leave Policy"} ];

	    suggestedActions = helpers.suggestionButtons(session ,builder, text, buttons);
	    session.send(suggestedActions);

	})
	.matches('Help', (session) => {
	    session.send(response["help"]["text"]);
	})
	.matches('Cancel', (session, args) => {
	    session.send('You reached Cancel intent, you said \'%s\'.', session.message.text);
	}).matches('main_option_selection',(session, args) =>{
		var intents = args.intent;
		//console.log("intents : "+JSON.stringify(args.entities));
		en_leave = builder.EntityRecognizer.findEntity(args.entities, 'en_leave');
		en_onboard = builder.EntityRecognizer.findEntity(args.entities, 'en_onboard');

		if(en_onboard !==null){
			suggestedActions = helpers.suggestionButtons(session, builder, response["option_onboard"]["text"], response["option_onboard"]["buttons"])
			session.send(suggestedActions);
		}else if(en_leave !== null){
			suggestedActions = helpers.suggestionButtons(session, builder, response["option_leave"]["text"], response["option_leave"]["buttons"])
			session.send(suggestedActions);
		}else{
			session.send("What's that ? I couldn't understand");
		}
	}).matches("annual_leaves",(session, args) =>{
		session.send(response["annual_leaves"]["text"]);
		session.endDialog();
	}).matches("sick_leave", (session, args)=>{
		session.send(response["sick_leaves"]["text"]);
	}).matches("apply_leave",[
		(session, args,  next)=>{
			//session.send(JSON.stringify(args));
			session.conversationData.scenario = 0;
			session.conversationData.lv_start_date = null;
			session.conversationData.lv_end_date = null;


			en_date = builder.EntityRecognizer.findEntity(args.entities, 'builtin.datetimeV2.date');
			en_date_range = builder.EntityRecognizer.findEntity(args.entities, 'builtin.datetimeV2.daterange');
			
			//console.log("en_date : "+JSON.stringify(en_date));
			//console.log("en_date_range : "+JSON.stringify(en_date_range));

			var date_value, start_date_value, end_date_value = null;

			if(en_date !== null){
				try{
					session.conversationData.lv_start_date = date_value = en_date["resolution"]["values"][0]["value"];

				}catch(e){}			
			}
			if(en_date_range !== null){
				try{
					session.conversationData.lv_start_date = start_date_value = en_date_range["resolution"]["values"][0]["start"];
					session.conversationData.lv_end_date = end_date_value = en_date_range["resolution"]["values"][0]["end"];
				}catch(e){}
				console.log("start_date_value : "+start_date_value);
			}

			if(start_date_value && start_date_value !== null && start_date_value !==undefined && end_date_value && end_date_value!==null && end_date_value!==undefined){

				session.conversationData.scenario = 1;
				let text = response["date_range_text"]["text"].replace("{date1}",start_date_value).replace("{date2}",end_date_value);	
				let buttons = response["date_range_text"]["buttons"];
				let confirmation = helpers.suggestionButtons(session,builder, text, buttons);
				builder.Prompts.text(session,confirmation);

			}
			else if(date_value!==null && date_value!==undefined && end_date_value!==null && end_date_value!==undefined){

				session.conversationData.scenario = 2;
				let text = response["date_range_text"]["text"].replace("{date1}",date_value).replace("{date2}",end_date_value);	
				let buttons = response["date_range_text"]["buttons"];
				let confirmation = helpers.suggestionButtons(session,builder, text, buttons);
				builder.Prompts.text(session,confirmation);
			}
			else if(date_value!==null && date_value!==undefined){

				session.conversationData.scenario = 3;
				let text = response["start_date_text"]["text"].replace("{date}",date_value);	
				let buttons = response["start_date_text"]["buttons"];
				let confirmation = helpers.suggestionButtons(session,builder, text, buttons);
				builder.Prompts.text(session,confirmation);

			}else{

				session.conversationData.scenario = 0;
				next();
			}
		},

		(session, results, next)=>{
			console.log("scenario : "+session.conversationData.scenario);
			let userRes = results.response;
			try{
				userRes = userRes.toLowerCase();
			}catch(e){}
			if(userRes == "yes"){

				switch(session.conversationData.scenario){
					case 0 : session.beginDialog("promptDates");
							break;
					case 1 : next(); //session.send("Thanks for the confirmation.");
							break;
					case 2 : next(); //session.send("Thanks for the confirmation.");
							break;
					case 3 : session.beginDialog("promptDates");
							break;
					default : session.beginDialog("promptDates");
				}
			}else{
				session.beginDialog("promptDates");
			}
		},
		(session, results)=>{
			let lv_start_date = session.conversationData.lv_start_date;
			let lv_end_date = session.conversationData.lv_end_date;
			console.log("lv_start_date : "+lv_start_date);
			try{
				lv_start_date = lv_start_date["resolution"]["start"];
				lv_end_date = lv_end_date["resolution"]["start"];
			}catch(e){}

			text = "Your request for leave is from "+lv_start_date+" till "+lv_end_date+"Confirm to proceed.";
			cnfrm_options = response["date_confirm"]["buttons"];
			cnfrm_msg = helpers.suggestionButtons(session, builder, text, cnfrm_options);
			builder.Prompts.text(session,cnfrm_msg);
		},
		(session, results) => {
			let userRes =  results.response;
			let lv_start_date = session.conversationData.lv_start_date;
			let lv_end_date = session.conversationData.lv_end_date;
			try{ userRes =userRes.toLowerCase(); }catch(e){}

			try{
				lv_start_date = lv_start_date["resolution"]["start"];
				lv_end_date = lv_end_date["resolution"]["start"];
			}catch(e){ }
			

			session.conversationData.lv_start_date = null;
			session.conversationData.lv_end_date = null;
			session.conversationData.scenario = 0;

			if(userRes == "yes"){
				session.send(response["leave_applied"]["text"].replace("{date1}",lv_start_date).replace("{date2}",lv_end_date))
			}else{
				session.send("Request cancelled.");
			}
			session.endDialog();
		}
	]).matches("my_leave_balance", (session) => {
		let text = response["my_leave_balance"]["text"];
		let hints = response["my_leave_balance"]["buttons"];
		let botResponse = helpers.suggestionButtons(session, builder, text, hints);
		session.send(botResponse);
	}).matches("ob_doc_required",(session, args)=>{
		session.send(response["ob_doc_required"]["text"]);
	}).matches("my_profile",(session,args) => {
		session.send(response["ob_my_profile"]["text"]);
	});

	/*
	.matches('<yourIntent>')... See details at http://docs.botframework.com/builder/node/guides/understanding-natural-language/
	.onDefault((session) => {
	    session.send('Sorry, I did not understand \'%s\'.', session.message.text);
	});
	*/

	bot.dialog('/', intents);  
	bot.dialog("promptDates",[
		(session) => {
			session.dialogData.lv_input_type = null;
			let lv_start_date = session.conversationData.lv_start_date;
			let lv_end_date = session.conversationData.lv_end_date;
			if(lv_start_date == null && lv_end_date == null){
				// prompt for both start & end date.
				session.dialogData.lv_input_type = 1;
				builder.Prompts.time(session,response["promp_startDate_text"]["text"]);
			}else if(lv_start_date == null){
				// prompt only for start date
				session.dialogData.lv_input_type =2
				builder.Prompts.time(session,response["promp_startDate_text"]["text"]);
			}else{
				// prompt only for end date
				session.dialogData.lv_input_type=3;
				builder.Prompts.time(session,response["promp_endDate_text"]["text"]);
			}
		},
		(session, results, next) => {
			console.log("session.dialogData.lv_input_type : "+session.dialogData.lv_input_type);
			if(session.dialogData.lv_input_type == 1){
				session.conversationData.lv_start_date = results.response;
				next();
			}else if(session.dialogData.lv_input_type == 2){
				session.conversationData.lv_start_date = results.response;
				session.endDialog();
			}else{
				session.conversationData.lv_end_date = results.response;
				session.endDialog();
			}
		},
		(session, results) => {
			builder.Prompts.time(session,response["promp_endDate_text"]["text"]);
		},
		(session, results) => {
			session.conversationData.lv_end_date = results.response;
			session.send("From : "+JSON.stringify(session.conversationData.lv_start_date)+"<br>Till : "+JSON.stringify(session.conversationData.lv_end_date));
			session.endDialog();
		}
	]);	



	// view to render bot on web page
	var botFrameHTML = "<iframe style='height: 500px;float: right;margin-right: 45%;' src='https://webchat.botframework.com/embed/CG-HR-BOT-POC?s=LB8Pb6C_Ykg.cwA.ebo.pyTHy-QdA-l-yzHcmC9J-zJFEIuFbM4Bma4TA-by2Q8'></iframe>";
	server.get("view/HRBot",(req, res, next)=>{
		res.writeHead(200, {
		  'Content-Length': Buffer.byteLength(botFrameHTML),
		  'Content-Type': 'text/html'
		});
		res.write(botFrameHTML);
		res.end();
	});

	var botUIHTML = fs.readFileSync("D-Line-API/index.html","utf8");
	server.get("D-Line-API/",(req, res, next)=>{
		res.writeHead(200, {
		  'Content-Length': Buffer.byteLength(botUIHTML),
		  'Content-Type': 'text/html'
		});
		res.write(botUIHTML);
		res.end();
	});