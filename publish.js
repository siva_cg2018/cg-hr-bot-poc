var zipFolder = require('zip-folder');
var path = require('path');
var fs = require('fs');
var request = require('request');

var rootFolder = path.resolve('.');
var zipPath = path.resolve(rootFolder, '../cg-hr-bot-poc.zip');
var kuduApi = 'https://cg-hr-bot-poc.scm.azurewebsites.net/api/zip/site/wwwroot';
var userName = '$cg-hr-bot-poc';
var password = 'gW6oTCseJRxrpvK3LMYfRDBBJfwZbt0FXtuJun7B0un0c86gWxtK9BX30QPm';

function uploadZip(callback) {
  fs.createReadStream(zipPath).pipe(request.put(kuduApi, {
    auth: {
      username: userName,
      password: password,
      sendImmediately: true
    },
    headers: {
      "Content-Type": "applicaton/zip"
    }
  }))
  .on('response', function(resp){
    if (resp.statusCode >= 200 && resp.statusCode < 300) {
      fs.unlink(zipPath);
      callback(null);
    } else if (resp.statusCode >= 400) {
      callback(resp);
    }
  })
  .on('error', function(err) {
    callback(err)
  });
}

function publish(callback) {
  zipFolder(rootFolder, zipPath, function(err) {
    if (!err) {
      uploadZip(callback);
    } else {
      callback(err);
    }
  })
}

publish(function(err) {
  if (!err) {
    console.log('cg-hr-bot-poc publish');
  } else {
    console.error('failed to publish cg-hr-bot-poc', err);
  }
});